﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        //Массив букв
        char [] masSimbols = new char[] { 'А','а','Б','б','В','в','Г','г','Д','д','Е','е','Ё','ё','Ж','ж','З','з','И','и','Й','й','К','к','Л','л','М','м','Н','н','О','о','П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ч','ч','Ш','ш','Щ','щ','Ъ','ъ','Ы','ы','Ь','ь','Э','э','Ю','ю','Я','я' };
        //Входной текст
        string text = "";
        //Вывод информации по букве
        string String = "";
        int count = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            text = textBox1.Text;
            calculate();
            
            
        }

        void calculate()
        {
            //Количество символов во входном тексте
            float countSimbolsInInputText = text.Length;
            //Проход по массиву символов
            for (int i = 0; i < masSimbols.Length; i++)
            {
                //Количество вхождений данного символа   
                float countSimbols = new Regex(masSimbols[i].ToString()).Matches(text).Count;
                //Если символ содержится
                if(countSimbols!=0){
                    //Частота
                    float frequency = countSimbols/countSimbolsInInputText;
                    //Вывод пользователю строк
                    textAnaliz += "Cимвол: " + masSimbols[i] + "\t Количество: " + countSimbols + "\t Частота : " + Math.Round(frequency, 3) + "\n";
                    textBox2.AppendText(textAnaliz);
                    textAnaliz = "";
                }
                
            }
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void metod(object sender, EventArgs e)
        {
            
        }
    }
}
